FROM node 

WORKDIR /react-notes-app-master/src

COPY . .

RUN npm i

EXPOSE 3000

CMD ["npm", "run", "start"]